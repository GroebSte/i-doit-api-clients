<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace declaration
 */
namespace idoit\Api\CMDB;

/**
 * Namespace alias
 */
use idoit\Api\Base;
use idoit\Api\InvalidParamException;

/**
 * Class Category
 *
 * @package idoit\Api\CMDB
 */
class Category
	extends Base
{
	const TypeSpecific = 's';
	const TypeGlobal   = 'g';

	/**
	 * @param string $p_categoryConst
	 */
	public function getStructure($p_categoryConst)
	{
		if (!is_numeric($p_categoryConst))
		{
			return $this->prepare(
				Methods::ReadCategoryStructure,
				array(
					'category' => $p_categoryConst
				)
			)->send();
		} else {
		    throw new InvalidParamException('$p_categoryConst should be a string representing the category\'s constant.');
		}
	}

	/**
	 * @param int $objectID
	 * @param int $p_categoryID
	 *
	 * @return mixed
	 */
	public function get($objectID, $p_categoryID)
	{
		return $this->prepare(
            Methods::ReadCategory,
            array(
	            'objID'    => $objectID,
	            'category' => $p_categoryID
            )
		)->send();
	}

	/**
	 * @param int                           $objectID
	 * @param \idoit\Api\CMDB\Category\Data $data
	 *
	 * @return mixed
	 */
	public function add($objectID, \idoit\Api\CMDB\Category\Data $data)
	{
		return $this->prepare(
            Methods::CreateCategory,
            array(
	            'objID'    => $objectID,
	            'category' => $data->getCategoryIdentifier(),
	            'data'     => $data->toArray()
            )
		)->send();
	}

	/**
	 * @param int                           $objectID     (e.g. 100)
	 * @param int                           $categoryID   (e.g. 1)
	 * @param \idoit\Api\CMDB\Category\Data $data
	 *
	 * @return mixed
	 */
	public function update($objectID, $categoryID, \idoit\Api\CMDB\Category\Data $data)
	{
		$data['id'] = $categoryID;

		return $this->prepare(
            Methods::UpdateCategory,
            array(
	            'objID'    => $objectID,
	            'category' => $data->getCategoryIdentifier(),
	            'data'     => $data->toArray()
            )
		)->send();
	}

	/**
	 * @param int    $objectID
	 * @param string $categoryConstant
	 * @param int    $categoryID
	 *
	 * @return mixed
	 */
	public function delete($objectID, $categoryConstant, $categoryID)
	{
		$data['id'] = $categoryID;

		return $this->prepare(
            Methods::DeleteCategory,
            array(
                 'objID'    => $objectID,
                 'category' => $categoryConstant,
                 'cateID'   => $categoryID
            )
		)->send();
	}

}
