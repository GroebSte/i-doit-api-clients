<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api;

class Client
{
    /**
     * @var Connection
     */
    public $connection;

    /**
     * @var Log
     */
    public $log;

    /**
     * @var \idoit\Api\Language\Base
     */
    public $language = null;

    /**
     * Creates a session and returns it's session id
     *
     * @return string|null
     */
    public function createSession()
    {
        if (Request::factory($this, 'idoit.login')->send())
        {
            $headers = $this->connection->jsonRpc->getHeadersOfLastRequest();

            if (isset($headers['X-RPC-Auth-Session']))
            {
                return $headers['X-RPC-Auth-Session'];
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function closeSession()
    {
        if (Request::factory($this, 'idoit.logout')->send())
        {
            return true;
        }

        return false;
    }

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection, \idoit\Api\Language\Base $language = null)
    {
        $this->log        = new Log();
        $this->connection = $connection;
        $this->log->message('Initializing connection to ' . $this->connection->jsonRpc->getURL(), Log::DEBUG);

        $this->language = $language ? $language : new \idoit\Api\Language\English();
    }
}