<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/* --------------------------------------------------------- */
/* Index */
/* --------------------------------------------------------- */
$f3->route(
	'GET /',
	function(Base $f3)
	{
		$apiObjTypeGroup = new \idoit\Api\CMDB\ObjectTypeGroup($f3->get('apiClient'));

		$objTypeGroups = $apiObjTypeGroup->getAll();

		$data = array();
		foreach ($objTypeGroups as $group)
		{
			$data[] = array(
				'title' => $group['title'],
				'link'  => $f3->get('www') . 'objecttype/' . $group['id'],
				'icon'  => 'pages'
			);
		}

		$f3->set('data', $data);

	}
);

/* --------------------------------------------------------- */
/* Search */
/* --------------------------------------------------------- */
$f3->route(
	'GET /search',
	function(Base $f3)
	{
		if (isset($_GET['s']))
		{
			$apiObjects    = new \idoit\Api\CMDB\Object($f3->get('apiClient'));
			$searchResults = $apiObjects->search($_GET['s'] . '%');

			$data = array();
			if ($searchResults && is_array($searchResults))
			{
				foreach ($searchResults as $object)
				{
					$data[] = array(
						'title' => $object['title'],
					    'link'  => $f3->get('www') . 'object/' . $object['id'],
						'icon'  => 'pages'
					);
				}
			}

			$f3->set('data', $data);
		}

		$f3->set('content', 'ui/search.html');

	}
);

/* --------------------------------------------------------- */
/* Objecttypes */
/* --------------------------------------------------------- */
$f3->route(
	'GET /objecttype/@typeGroup',
	function(Base $f3, $args)
	{
		if (isset($args['typeGroup']) && $args['typeGroup'] > 0)
		{
			$apiObjectType = new \idoit\Api\CMDB\ObjectType($f3->get('apiClient'));
			$objectTypes   = $apiObjectType->getAllByGroup($args['typeGroup']);

			$data = array();

			if (is_array($objectTypes) && count($objectTypes) > 0)
			{
				usort(
					$objectTypes,
					'arrayCompare'
				);
				foreach ($objectTypes as $type)
				{
					$data[] = array(
						'title' => $type['title'],
						'badge' => (isset($type['objectcount']) ? $type['objectcount'] : ''),
						'link'  => $f3->get('www') . 'objects/' . $type['id'],
						'icon'  => 'pages'
					);
				}
			}

			$f3->set('data', $data);
			$f3->set('enableBackButton', true);
		} else {
		    throw new Exception('Parameter error. Specify an object-type!');
		}
	}
);

/* --------------------------------------------------------- */
/* Objects */
/* --------------------------------------------------------- */
$f3->route(
	'GET /objects/@objectType',
	function(Base $f3, $args)
	{
		if ($args['objectType'] > 0)
		{
			$apiObjectType = new \idoit\Api\CMDB\ObjectType($f3->get('apiClient'));
			$objectType    = $apiObjectType->get($args['objectType']);

			$apiObject = new \idoit\Api\CMDB\Object($f3->get('apiClient'));
			$objects   = $apiObject->getAllByType($args['objectType']);

			if (isset($objectType['title']))
			{
				$f3->set('title', $objectType['title'] . ' (' . count($objects) . ')');
			}

			$data = array();

			if (is_array($objects) && count($objects) > 0)
			{
				usort(
					$objects,
					'arrayCompare'
				);

				foreach ($objects as $object)
				{
					$data[] = array(
						'title' => $object['title'],
						'link'  => $f3->get('www') . 'object/' . $object['id'],
						'icon'  => 'pages'
					);
				}
			}

			$f3->set('data', $data);
			$f3->set('enableBackButton', true);
		} else {
		    throw new Exception('Parameter error. Specify an object!');
		}
	}
);

/* --------------------------------------------------------- */
/* Object */
/* --------------------------------------------------------- */
$f3->route(
	'GET /object/@object',
	function(Base $f3, $args)
	{
		if ($args['object'] > 0)
		{
			$apiObject = new \idoit\Api\CMDB\Object($f3->get('apiClient'));
			$object = $apiObject->get($args['object']);

			if ($object && isset($object['title']))
			{
				$f3->set('title', $object['title']);
				$f3->set('enableBackButton', true);

				$apiObjectType = new \idoit\Api\CMDB\ObjectType($f3->get('apiClient'));
				$categories    = $apiObjectType->getAssignedCategories($object['objecttype']);

				$data = array();

				if (isset($categories['cats']) && is_array($categories['cats']))
				{
					usort(
						$categories['cats'],
						'arrayCompare'
					);
					foreach ($categories['cats'] as $cats)
					{
						$data[] = array(
							'title' => $cats['title'],
							'link'  => $f3->get('www') . 'object/' . $args['object'] . '/' . $cats['const'],
							'icon'  => 'pages'
						);
					}
				}

				if (isset($categories['catg']) && is_array($categories['catg']))
				{
					usort(
						$categories['catg'],
						'arrayCompare'
					);
					foreach ($categories['catg'] as $catg)
					{
						$data[] = array(
							'title' => $catg['title'],
							'link'  => $f3->get('www') . 'object/' . $args['object'] . '/' . $catg['const'],
							'icon'  => 'pages'
						);
					}
				}

				$f3->set('data', $data);
			}
		} else {
		    throw new Exception('Parameter error. Specify an object!');
		}
	}
);

/* --------------------------------------------------------- */
/* Object */
/* --------------------------------------------------------- */
$f3->route(
	'GET /object/@object/@category',
	function(Base $f3, $args)
	{
		$f3->set('enableBackButton', true);

		if ($args['object'] > 0 && strlen($args['category']) > 0)
		{
			$uiData = array(); $i = 0;

			try
			{
				$apiCategory       = new idoit\Api\CMDB\Category($f3->get('apiClient'));
				$categoryData      = $apiCategory->get($args['object'], $args['category']);
				$categoryStructure = $apiCategory->getStructure($args['category']);

				$apiObject = new \idoit\Api\CMDB\Object($f3->get('apiClient'));
				$object    = $apiObject->get($args['object']);
				$f3->set('title', $object['title']);

				if (count($categoryData) > 0 && is_array($categoryStructure))
				{
					foreach ($categoryData as $data)
					{
						foreach ($categoryStructure as $key => $structure)
						{
							$title = isset($structure['title']) ? $structure['title'] : $structure['info']['description'];

							if (isset($data[$key]) && $data[$key])
							{
								$uiData[$i][$title] = formatProperty($data[$key]);
							} else
							{
								//$uiData[$i][$title] = '';
							}
						}
						$i++;
					}
				}
				$f3->set('content', 'ui/datalisting.html');
			}
			catch (\idoit\Api\Exception $e)
			{
				$f3->set('error', $e->getError() . ': ' . $e->getErrorDetails());
			}

			$f3->set('data', $uiData);

			/*
			if ($object && isset($object['title']))
			{
				$f3->set('enableBackButton', true);
				$f3->set('content', 'ui/datalisting.html');

				$data[] = array(
					'ID'           => $object['id'],
					'Title'        => $object['title'],
					'Objecttype'   => $object['type_title'],
					'SYS-ID'       => $object['sysid'],
					'CMDB Status'  => ucfirst($object['cmdb_status_title']),
					'Created'      => $object['created'],
					'Last Updated' => $object['updated'],
				);

				$f3->set('data', $data);
			}
			*/
        }
        else throw new Exception('Parameter error. Specify an object!');
    }
);