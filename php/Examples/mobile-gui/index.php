<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2016 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\Connection as ApiConnection;

try
{
	include_once(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'apiclient.php');
}
catch (\idoit\Api\InfoException $e)
{
	// Ignore info messages
}

try
{
	/**
	 * Include composer autoloader
	 */
	include_once('vendor/autoload.php');

	/**
	 * Inlcude and instantiate fatfree framework
	 */
	$f3 = include_once('vendor/bcosca/fatfree/lib/base.php');


	/* --------------------------------------------------------- */
	/* Initalize */
	/* --------------------------------------------------------- */
	include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config.php');

	/* Api related config */
	\idoit\Api\Config::$jsonRpcDebug = false;

	/**
	 * Set framework variables
	 */
	$f3->set('apiClient', new ApiClient(new ApiConnection($api_entry_point, $api_key)));
	$f3->set('www', rtrim(str_replace('index.php', '', $_SERVER['SCRIPT_NAME']), '/') . '/');
	$f3->set('theme', stripos($_SERVER['HTTP_USER_AGENT'], "Android") ? 'andoid' : 'ios');

	/**
	 * Set default content and title
	 */
	$f3->set('content', 'ui/listing.html');
	$f3->set('title', 'i-doit Mobile');

	/* --------------------------------------------------------- */
	/* Helper functions */
	/* --------------------------------------------------------- */
	/**
	 * @param $p_x
	 * @param $p_y
	 *
	 * @return bool|int
	 */
	function arrayCompare($p_x, $p_y)
	{
		if (is_array($p_x) && isset($p_x['title']) && isset($p_y['title']))
		{
					return strnatcasecmp($p_x['title'], $p_y['title']);
		} else if (is_string($p_x))
		{
		    return strnatcasecmp($p_x, $p_y);
		} else {
		    return false;
		}
	}

	/**
	 * @param $p_mixed
	 *
	 * @return string
	 */
	function formatProperty($p_mixed)
	{
		if (is_array($p_mixed))
		{
			if (isset($p_mixed['value']) && !is_numeric($p_mixed['value']))
			{
				if (($decode = base64_decode($p_mixed['value'])))
				{
					return '<img style="height:250px;" src="data:image/gif;base64,' . $p_mixed['value'] . '" />';
				} else {
				    return $p_mixed['value'];
				}
			} else if (isset($p_mixed['title']))
			{
				return $p_mixed['title'];
			}
		} else if (is_scalar($p_mixed))
		{
			return $p_mixed;
		}

		return '';
	}

	/* --------------------------------------------------------- */
	/* Routing */
	/* --------------------------------------------------------- */
	include_once('application.php');

	/**
	 * Run Fat-Free engine
	 */
	$f3->run();

	/**
	 * Display templates
	 */
	$template = new Template;
	echo $template->render('ui/index.html');

}
catch (\idoit\Api\InfoException $e)
{
	// Ignore info messages
}
catch (Exception $e)
{
	// Handle error message
	print_r($e->getMessage());
	echo "\n";
}