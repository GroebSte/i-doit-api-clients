![i-doit](http://www.i-doit.com/fileadmin/templates/images/logo.gif)

#i-doit API Example: Mobile GUI

This is a ~300 LOC php example of how to interact with the i-doit API Client and present the data in a nice interface.

1) Load dependencies with composer (https://getcomposer.org/)

```
composer install
```

2) Call index.php in your web browser