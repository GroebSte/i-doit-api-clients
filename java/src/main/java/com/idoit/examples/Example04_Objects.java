/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.examples;

import com.idoit.api.IdoitClient;
import com.idoit.api.IdoitClientException;
import com.idoit.api.cmdb.CMDBObject;
import com.idoit.api.cmdb.ObjectTypeConstants;
import java.util.List;
import org.json.JSONException;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class Example04_Objects {

    public static void main(String[] args) throws JSONException {
        try {
            // URL und API-KEY in der Configuration Datei anpassen!
            // Client erstellen
            IdoitClient client = new IdoitClient(Configuration.IDOIT_URL, Configuration.API_KEY);

            // Login
            client.login("admin", "admin");

            // Objekt anlegen
            CMDBObject cmdbObject = client.object().create(ObjectTypeConstants.TYPE_SERVER, "Test-Server");

            // Objekt lesen
            CMDBObject cmdbObject2 = client.object().read(cmdbObject.getID());

            // Objekt updaten
            boolean update = client.object().update(cmdbObject.getID(), "Test-Server Update");

            // Objekt löschen
            boolean delete = client.object().delete(cmdbObject.getID());

            // Beispiele für verschiedene Objekt Abfragen (Fluent Interface)
            // 1. die ersten 50 Objekte vom Typ Server
            List<CMDBObject> cmdbObjects = client.objects().filterType(ObjectTypeConstants.TYPE_SERVER).pageSize(50).page(0).read();

            // 2. ein Objekte über die id abfragen
            List<CMDBObject> cmdbObjects2 = client.objects().filterIDs("32").read();

            // 3. mehrere Objekte über ihre id abfragen
            List<CMDBObject> cmdbObjects3 = client.objects().filterIDs("32", "46", "213").read();

            // 4. die ersten 10 Objekte vom Typ Server deren Titel mit "Server" anfängt sortiert nach Titel in umgekehrter Reihenfolge
            List<CMDBObject> cmdbObjects4 = client.objects().filterType(ObjectTypeConstants.TYPE_SERVER).filterTitle("Server%").orderByTitle().sortDescending().pageSize(10).read();

            // Logout
            client.logout();

        } catch (IdoitClientException ex) {
            System.out.println("RPC-ERROR");
            System.out.println("---------");
            System.out.println("message: " + ex.getMessage());
            System.out.println("error: " + ex.getError());
            System.out.println("code: " + ex.getCode());
        }
    }
}
