/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb.category;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBCategory {

    private Map<String, Value> valueMap;

    private Map<String, Value> getValueMap() {
        if (valueMap == null) {
            valueMap = new HashMap<>();
        }
        return valueMap;
    }

    public Collection<Value> values() {
        return getValueMap().values();
    }

    public Value createValue(String key) {
        Value value = new Value(key);
        getValueMap().put(key, value);
        return value;
    }

    public Value getValue(String key) {
        return getValueMap().get(key);
    }

    public int countValues() {
        return getValueMap().keySet().size();
    }

    public String getID() {
        Value value = getValue("id");
        return value != null ? value.getTitle() : "0";
    }

    public String getObjID() {
        Value value = getValue("objID");
        return value != null ? value.getTitle() : "0";
    }

    public class Value {

        private final String key;
        private String title;
        private Map<String, String> propertyMap;

        public Value(String key) {
            this.key = key;
        }

        private Map<String, String> getPropertyMap() {
            if (propertyMap == null) {
                propertyMap = new HashMap<>();
            }
            return propertyMap;
        }

        public Collection<String> properties() {
            return getPropertyMap().values();
        }

        public void putProperty(String key, String value) {
            getPropertyMap().put(key, value);
        }

        public String getProperty(String propertyKey) {
            return getPropertyMap().get(propertyKey);
        }

        public int countProperties() {
            return getPropertyMap().size();
        }

        public String getID() {
            return getProperty("id");
        }

        public String getKey() {
            return key;
        }

        public String getTitle() {
            String currentTitle = isSimpleValue() ? this.title : getProperty("title");
            if (currentTitle == null) {
                currentTitle = "";
            }
            return currentTitle;
        }

        public void setTitle(String title) {
            if (isSimpleValue()) {
                this.title = title;
            } else {
                getPropertyMap().put("title", title);
            }
        }

        public String getPropType() {
            return getProperty("prop_type");
        }

        public boolean isSimpleValue() {
            return getPropertyMap().isEmpty();
        }
    }
}
