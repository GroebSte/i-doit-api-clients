/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBObjectTypeGroups {

    private Map<String, Group> groupMap;

    public CMDBObjectTypeGroups(JSONArray jsonArray) {
        init(jsonArray);
    }

    private Map<String, Group> getGroupMap() {
        if (groupMap == null) {
            groupMap = new HashMap<>();
        }
        return groupMap;
    }

    public Collection<Group> groups() {
        return getGroupMap().values();
    }

    public boolean hasGroup(String id) {
        return getGroupMap().containsKey(id);
    }

    public Group getGroup(String id) {
        return getGroupMap().get(id);
    }

    public int countGroupes() {
        return getGroupMap().size();
    }

    private void init(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            Group group = new Group(jsonArray.optJSONObject(i));
            getGroupMap().put(group.getID(), group);
        }
    }

    public class Group {

        public Group(JSONObject jsonObject) {
            init(jsonObject);
        }

        private String id;

        private String title;

        private String constant;

        private String status;

        public String getID() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getConstant() {
            return constant;
        }

        public String getStatus() {
            return status;
        }

        private void init(JSONObject jsonObject) {
            this.id = jsonObject.optString("id", "");
            this.title = jsonObject.optString("title", "");
            this.constant = jsonObject.optString("const", "");
            this.status = jsonObject.optString("status", "");
        }
    }
}
