/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb.category;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBCategoryInfo {

    private String constant;
    private Map<String, CategoryAttribute> categoryAttributeMap;

    public String getConstant() {
        if (constant == null) {
            constant = "";
        }
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }
    
    private Map<String, CategoryAttribute> getCategoryAttributeMap() {
        if (categoryAttributeMap == null) {
            categoryAttributeMap = new HashMap<>();
        }
        return categoryAttributeMap;
    }

    public Collection<CategoryAttribute> attributes() {
        return getCategoryAttributeMap().values();
    }

    public CategoryAttribute getAttribute(String title) {
        return getCategoryAttributeMap().get(title);
    }

    public void addAttribute(CategoryAttribute attribute) {
        if (attribute != null) {
            getCategoryAttributeMap().put(attribute.getTitle(), attribute);
        }
    }
    
    public int countAttributes() {
        return getCategoryAttributeMap().size();
    }
}
