/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api.cmdb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBObject {

    private String id;
    private String title;
    private String imageUrl;
    private String sysID;
    private String objectType;
    private String objectTypeTitle;
    private String objectTypeIconPath;
    private String status;
    private String cmdbStatus;
    private String cmdbStatusTitle;
    private Date created;
    private Date updated;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSysID() {
        return sysID;
    }

    public void setSysID(String sysID) {
        this.sysID = sysID;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectTypeTitle() {
        return objectTypeTitle;
    }

    public void setObjectTypeTitle(String objectTypeTitle) {
        this.objectTypeTitle = objectTypeTitle;
    }

    public String getObjectTypeIconPath() {
        return objectTypeIconPath;
    }

    public void setObjectTypeIconPath(String objectTypeIconPath) {
        this.objectTypeIconPath = objectTypeIconPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCMDBStatus() {
        return cmdbStatus;
    }

    public void setCMDBStatus(String cmdbStatus) {
        this.cmdbStatus = cmdbStatus;
    }

    public String getCMDBStatusTitle() {
        return cmdbStatusTitle;
    }

    public void setCMDBStatusTitle(String cmdbStatusTitle) {
        this.cmdbStatusTitle = cmdbStatusTitle;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    public void setCreated(String dateString) {
        try {
            this.created = dateFormat.parse(dateString);
        } catch (ParseException ex) {
            this.created = new Date();
        }
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    public void setUpdated(String dateString) {
        try {
            this.updated = dateFormat.parse(dateString);
        } catch (ParseException ex) {
            this.updated = new Date();
        }
    }
}
