/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.CMDBObject;
import com.idoit.api.cmdb.ObjectTypeConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class ObjectsModule implements MethodConstants {

    private final IdoitClient client;
    private final static String ORDER_BY_ID = "id";
    private final static String ORDER_BY_TITLE = "title";
    private final static String ORDER_BY_TYPE = "type";
    private final static String ORDER_BY_TYPE_TITLE = "type_title";
    private final static String ORDER_BY_SYS_ID = "sysid";
    private final static String ORDER_BY_FIRST_NAME = "first_name";
    private final static String ORDER_BY_LAST_NAME = "last_name";
    private final static String ORDER_BY_EMAIL = "email";

    private JSONArray filterIDs;
    private String filterTitle;
    private String filterType;
    private String filterTypeTitle;
    private String filterSysID;
    private String filterFirstName;
    private String filterLastName;
    private String filterEmail;

    private String orderBy = "";
    private int pageSize = 0;
    private int page = 0;
    private String sort = "asc";

    public ObjectsModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Read the objects.
     *
     * @see CMDBObject
     * @return Collection of all objects
     * @throws IdoitClientException
     */
    public List<CMDBObject> read() throws IdoitClientException {
        JSONObject responseObject = client.sendRequest(CMDB_OBJECTS_READ, createParams(), "1");
        return createCMDBObjects(responseObject);
    }

    /**
     * Set the page size for objects query.
     *
     * @param pageSize the page size
     * @return ObjectsModule
     */
    public ObjectsModule pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    /**
     * Set the number of the page for objects query.
     *
     * @param page the page number
     * @return ObjectsModule
     */
    public ObjectsModule page(int page) {
        this.page = page;
        return this;
    }

    /**
     * Filter objects with the given ids.
     *
     * @param ids object IDs
     * @return ObjectsModule
     */
    public ObjectsModule filterIDs(String... ids) {
        this.filterIDs = new JSONArray(Arrays.asList(ids));
        return this;
    }

    /**
     * Filter objects with the given title.
     *
     * @param title object title
     * @return ObjectsModule
     */
    public ObjectsModule filterTitle(String title) {
        this.filterTitle = title;
        return this;
    }

    /**
     * Filter objects with the given type.
     *
     * @param type object type
     * @see ObjectTypeConstants
     * @return ObjectsModule
     */
    public ObjectsModule filterType(String type) {
        this.filterType = type;
        return this;
    }

    /**
     * Filter objects with the given type title.
     *
     * @param typeTitle object type title
     * @return ObjectsModule
     */
    public ObjectsModule filterTypeTitle(String typeTitle) {
        this.filterTypeTitle = typeTitle;
        return this;
    }

    /**
     * Filter objects with the given sysID.
     *
     * @param sysID object sysID
     * @return ObjectsModule
     */
    public ObjectsModule filterSysID(String sysID) {
        this.filterSysID = sysID;
        return this;
    }

    /**
     * Filter persons with the given first name.
     *
     * @param firstName person first name
     * @return ObjectsModule
     */
    public ObjectsModule filterFirstName(String firstName) {
        this.filterFirstName = firstName;
        return this;
    }

    /**
     * Filter persons with the given last name.
     *
     * @param lastName person last name
     * @return ObjectsModule
     */
    public ObjectsModule filterLastName(String lastName) {
        this.filterLastName = lastName;
        return this;
    }

    /**
     * Filter persons with the given email.
     *
     * @param email person email
     * @return ObjectsModule
     */
    public ObjectsModule filterEmail(String email) {
        this.filterEmail = email;
        return this;
    }

    /**
     * Order the objects by ID.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByID() {
        this.orderBy = ORDER_BY_ID;
        return this;
    }

    /**
     * Order the objects by title.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByTitle() {
        this.orderBy = ORDER_BY_TITLE;
        return this;
    }

    /**
     * Order the objects by object type.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByType() {
        this.orderBy = ORDER_BY_TYPE;
        return this;
    }

    /**
     * Order the objects by object type title.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByTypeTitle() {
        this.orderBy = ORDER_BY_TYPE_TITLE;
        return this;
    }

    /**
     * Order the objects by sysID.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderBySysID() {
        this.orderBy = ORDER_BY_SYS_ID;
        return this;
    }

    /**
     * Order the objects by first name.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByFirstName() {
        this.orderBy = ORDER_BY_FIRST_NAME;
        return this;
    }

    /**
     * Order the objects by last name.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByLastName() {
        this.orderBy = ORDER_BY_LAST_NAME;
        return this;
    }

    /**
     * Order the objects by email address.
     *
     * @return ObjectsModule
     */
    public ObjectsModule orderByEmail() {
        this.orderBy = ORDER_BY_EMAIL;
        return this;
    }

    /**
     * Sort the objects ascending.
     *
     * @return ObjectsModule
     */
    public ObjectsModule sortAscending() {
        this.sort = "asc";
        return this;
    }

    /**
     * Sort the objects descending.
     *
     * @return ObjectsModule
     */
    public ObjectsModule sortDescending() {
        this.sort = "desc";
        return this;
    }

    private JSONObject createParams() throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            JSONObject filter = createFilter();
            if (filter.length() > 0) {
                params.put("filter", filter);
            }
            if (!orderBy.isEmpty()) {
                params.put("order_by", orderBy);
            }
            if (pageSize > 0) {
                int offset = page * pageSize;
                params.put("limit", String.format("%d,%d", offset, pageSize));
            }
            params.put("sort", sort);
            return params;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private JSONObject createFilter() throws IdoitClientException {
        try {
            JSONObject filter = new JSONObject();
            if (filterIDs != null) {
                filter.put("ids", filterIDs);
            }
            if (filterTitle != null && !filterTitle.isEmpty()) {
                filter.put("title", filterTitle);
            }
            if (filterType != null && !filterType.isEmpty()) {
                filter.put("type", filterType);
            }
            if (filterTypeTitle != null && !filterTypeTitle.isEmpty()) {
                filter.put("type_title", filterTypeTitle);
            }
            if (filterSysID != null && !filterSysID.isEmpty()) {
                filter.put("sysid", filterSysID);
            }
            if (filterFirstName != null && !filterFirstName.isEmpty()) {
                filter.put("first_name", filterFirstName);
            }
            if (filterLastName != null && !filterLastName.isEmpty()) {
                filter.put("last_name", filterLastName);
            }
            if (filterEmail != null && !filterEmail.isEmpty()) {
                filter.put("email", filterEmail);
            }
            return filter;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private List<CMDBObject> createCMDBObjects(JSONObject jsonObject) throws IdoitClientException {
        try {
            List<CMDBObject> cmdbObjects = new ArrayList<>();
            JSONArray resultObjects = jsonObject.getJSONArray("result");

            for (int i = 0; i < resultObjects.length(); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(i);
                CMDBObject cmdbObject = new CMDBObject();
                cmdbObject.setID(resultObject.optString("id", ""));
                cmdbObject.setTitle(resultObject.optString("title", ""));
                cmdbObject.setSysID(resultObject.optString("sysid", ""));
                cmdbObject.setObjectType(resultObject.optString("objecttype", ""));
                cmdbObject.setObjectTypeTitle(resultObject.optString("type_title", ""));
                cmdbObject.setObjectTypeIconPath(resultObject.optString("type_icon", ""));
                cmdbObject.setStatus(resultObject.optString("status", ""));
                cmdbObject.setCMDBStatus(resultObject.optString("cmdb_status", ""));
                cmdbObject.setCMDBStatusTitle(resultObject.optString("cmdb_status_title", ""));
                cmdbObject.setCreated(resultObject.optString("created", ""));
                cmdbObject.setUpdated(resultObject.optString("updated", ""));
                cmdbObject.setImageUrl(resultObject.optString("image", ""));
                cmdbObjects.add(cmdbObject);
            }
            return cmdbObjects;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
