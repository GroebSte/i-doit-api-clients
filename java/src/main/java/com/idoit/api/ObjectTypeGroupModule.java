/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.CMDBObjectTypeGroups;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class ObjectTypeGroupModule implements MethodConstants {

    private final IdoitClient client;
    private final static String ORDER_BY_ID = "id";
    private final static String ORDER_BY_TITLE = "title";
    private final static String ORDER_BY_STATUS = "status";
    private final static String ORDER_BY_CONSTANT = "constant";

    private String orderBy = "";
    private int pageSize = 0;
    private int page = 0;

    public ObjectTypeGroupModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Read the object type groups.
     *
     * @see CMDBObjectTypeGroups
     * @return CMDBObjectTypeGroups object contains all object type groups
     * @throws IdoitClientException
     */
    public CMDBObjectTypeGroups read() throws IdoitClientException {
        JSONObject responseObject = client.sendRequest(CMDB_OBJECT_TYPE_GROUPS, createParams(), "1");
        JSONArray resultObjects = responseObject.optJSONArray("result");
        return new CMDBObjectTypeGroups(resultObjects);
    }

    /**
     * Set the page size for object type groups query.
     *
     * @param pageSize the page size
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    /**
     * Set the number of the page for object type groups query.
     *
     * @param page the page number
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule page(int page) {
        this.page = page;
        return this;
    }

    /**
     * Order the object type groups by ID.
     *
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule orderByID() {
        this.orderBy = ORDER_BY_ID;
        return this;
    }

    /**
     * Order the object type groups by title.
     *
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule orderByTitle() {
        this.orderBy = ORDER_BY_TITLE;
        return this;
    }

    /**
     * Order the object type groups by status.
     *
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule orderByStatus() {
        this.orderBy = ORDER_BY_STATUS;
        return this;
    }

    /**
     * Order the object type groups by constant.
     *
     * @return ObjectTypeGroupModule
     */
    public ObjectTypeGroupModule orderByConstant() {
        this.orderBy = ORDER_BY_CONSTANT;
        return this;
    }

    private JSONObject createParams() throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            if (!orderBy.isEmpty()) {
                params.put("order_by", orderBy);
            }
            if (pageSize > 0) {
                int offset = page * pageSize;
                params.put("limit", String.format("%d,%d", offset, pageSize));
            }
            return params;
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }
}
