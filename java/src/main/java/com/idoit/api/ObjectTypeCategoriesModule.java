/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import com.idoit.api.cmdb.CMDBObjectTypeCategories;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class ObjectTypeCategoriesModule implements MethodConstants {

    private final IdoitClient client;

    public ObjectTypeCategoriesModule(IdoitClient client) {
        this.client = client;
    }

    /**
     * Reading categories of object types.
     *
     * @param objectType the object type ID or constant
     *
     * @see CMDBObjectTypeCategories
     * @return CMDBObjectTypeCategories object contains all categories for the
     * given object type
     * @throws IdoitClientException
     */
    public CMDBObjectTypeCategories read(String objectType) throws IdoitClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("type", objectType);
            JSONObject responseObject = client.sendRequest(CMDB_OBJECT_TYPE_CATEGORIES, params, "1");
            JSONObject resultObject = responseObject.optJSONObject("result");
            return new CMDBObjectTypeCategories(resultObject);
        } catch (JSONException ex) {
            throw new IdoitClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }
}
